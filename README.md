# Salt Lake DSA Website

NOTE: The instruction in this README were tested on a Linux system. It ought to all work with Windows, but who knows 🤷

# Running locally

## Pre-Reqs

- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

From the base directory of the project run: `docker-compose up` and the site should be available at http://localhost:8080/

# Developing

## Pre-Reqs

- 🐋[docker](https://docs.docker.com/install/)🐋
- [docker-compose](https://docs.docker.com/compose/install/)
- [npm](https://www.npmjs.com/get-npm) (Recommend using a node version manager like [nvm](https://github.com/nvm-sh/nvm) or if you on windows [nvm-windows](https://github.com/coreybutler/nvm-windows))
- From the /app directory run: `npm install`

There are two ways to locally develop the app (a third if you count debugging). Both ways result in a local mongodb instance (explorable at http://localhost:8081/) as well as a hot-reloadable version of the site running at http://localhost:8080/. The only difference is that with one method (the 'hard' way) you interact with docker and npm, wheras with the other method (the 'easy' way) you just run two npm commands.

# Easy way

From the /app directory run: `npm run start-dev`

# Easy way (with debugging)

Note: Currently the debugging should work automatically in [vscode](https://code.visualstudio.com/download). If you use another IDE run `npm run debug` from the /app directory and attach your debugger to port 9229

press the `F5` key!

NOTE: The debugger (if your in vscode) also supports hot-reloading, but sometimes it takes a few seconds for the debugger to re-attach to the restarted process.

# Hard Way

## Start a mongodb instance

From the base directory of the project run: `docker-compose --f ./docker-compose.dev.yaml up` and you should have a running mongodb adn mongo express instance. You can explore the database at http://localhost:8081/

## Run a hot-reloadable instance of the site for development

From the /app directory run: `npm run start-dev-app` and the site should be available at http://localhost:8080/
Since this is a dev isntance of the site any changes you make to the code should trigger a rebuild (which is really fast) and your changes should be reflected in the page after a refresh.
